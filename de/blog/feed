<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://amarok.kde.org"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Amarok blogs</title>
 <link>https://amarok.kde.org/de/blog</link>
 <description>

Amarok is a powerful music player for Linux, Unix and Windows with an intuitive interface. It makes playing the music you love and discovering new music easier than ever before - and it looks good doing it!

Discover what Amarok has to offer. And see where we want to take it.</description>
 <language>de</language>
<item>
 <title>Test</title>
 <link>https://amarok.kde.org/de/node/623</link>
 <description>&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot;&gt;&lt;p&gt;a test blog&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Sat, 07 Mar 2009 20:03:14 +0000</pubDate>
 <dc:creator>Ian</dc:creator>
 <guid isPermaLink="false">623 at https://amarok.kde.org</guid>
 <comments>https://amarok.kde.org/de/node/623#comments</comments>
</item>
<item>
 <title>The best feature of Amarok...</title>
 <link>https://amarok.kde.org/de/node/604</link>
 <description>&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot;&gt;&lt;p&gt;It seems in every version of Amarok 2 we have released there is at least one comment that sounds something like:&lt;br /&gt;
  &quot;But, the best feature of amarok 1.4 is still missing! Without --Insert one of the zillion and a half features Amarok 1.4 had-- Amarok 2.0 is useless!&quot; &lt;/p&gt;
&lt;p&gt;It is interesting for me to observe just how many &quot;best features&quot; Amarok 1.4 had, and also to observe exactly how many different ways Amarok was used.  One of the issues with Amarok 1 development was that Amarok was trying to be a jack of all trades, and not really mastering any of them.  This was well and good for the average Linux user, many of whom, when Amarok 1 was originally created, had no problem digging into the source to add their own little tweaks, features, or bug fixes.  However, it is a problem for people that just want to listen to music, and leave the text editors and compilers at the door. With Amarok 2, our goal was not just to have features, but to perfect them.&lt;/p&gt;
&lt;p&gt;To perfect features, we needed to limit our scope.  Amarok 1 included a lot of dump-and-run code, where a large feature was submitted by an interested user, the code was committed and shipped, and then we never heard from the author again.  This was the case with many of the media devices, and many of the engine backends.  &lt;/p&gt;
&lt;p&gt;We rely on phonon in amarok 2, so the engine issue is no longer one we have control over, although we are suffering from the results of incomplete phonon engines.  While it is all sorts of nice to get backends for windows and mac for free, they come with many issues still, and we are constantly closing bugs reported by users that use the gstreamer phonon backend, as it is extremely buggy and incomplete.  Unfortunately, many distributions ship this by default (due to patent issues) and as a result Amarok gets a reputation as being a lot less stable than it actually is on these distributions.  Hopefully this issue is resolved in the future.&lt;/p&gt;
&lt;p&gt;With regards to portable media devices, the ball is still in our park.  Alejandro (xevix) had a summer of code project for portable media device support, and is working now on generalizing the code so that it will be fairly simple to add new devices.  The shared infrastructure that Amarok 2 is built on (The Meta/Collection architecture) provides a familiar interface, so that even if Alejandro runs out of time (or interest) in the future, the code will be clear and understandable to whoever might pick up the pieces.  The next step will be to readd additional media devices to Amarok 2 using this infrastructure.  This should happen for 2.1 or 2.2, depending on various factors.&lt;/p&gt;
&lt;p&gt;As we are constantly being reminded, perfection is not a fire and forget process.  Code constantly improves (we hope) and mistakes we made in previous versions influence the next iteration that comes.  This is quite visually apparent with the playlist.  The Amarok 1.4 playlist was incredible to many people.  It had a a lot of functionality and all sorts of nifty features, but at the same time, it was entirely unmaintainable by the time Amarok 1.4 ended. See &lt;a href=&quot;http://websvn.kde.org/*checkout*/tags/amarok/1.4.9/multimedia/amarok/src/playlist.cpp?revision=795162&amp;amp;content-type=text%2Fplain&quot;&gt;this file&lt;/a&gt; if you don&#039;t believe me.  For Amarok 2 we knew the playlist was important, but we wanted to start over, using the new model/view features in Qt4, and make it look more visually appealing.  Ian&#039;s adventures are &lt;a href=&quot;http://amarok.kde.org/blog/archives/457-Status-of-QGraphicScenes-and-Delegates.html&quot;&gt;well&lt;/a&gt; &lt;a href=&quot;http://amarok.kde.org/blog/archives/454-A-Cry-for-Help-Combining-GraphicsView-and-Interview.html&quot;&gt;documented&lt;/a&gt;.  Nikolaj&#039;s adventures leading up to 2.0 were also well documented.  The system that was created for 2.0 was a far cry from perfect, (the various paint methods made me cry on more than one occasion) but it was an important step on the road to this whole perfection thing.  As Nikolaj more recent &lt;a href=&quot;http://amarok.kde.org/blog/archives/858-From-the-Post-2.0.0-Git-Vaults,-Part-2,-The-Playlist-Evolved.html&quot;&gt;blogged&lt;/a&gt;, Amarok 2.1 is moving a lot closer to the ideal.  I&#039;m sure it will have issues, but with testing and use it will hopefully evolve into what we originally intended, a kick-ass feature that puts the Amarok 1.4 playlist to shame.&lt;/p&gt;
&lt;p&gt;This has wandered pretty far afield from the point I didn&#039;t start with, but I suppose what I&#039;m getting at is the same thing that we keep saying, and because I like to hit dead horses, I&#039;ll say it again.  Amarok 2 is going to have many of the best features that Amarok 1 had.  It will not have all of them.  Our goal is to make a music player that is fun and enjoyable to use for a majority of the people.  If this means omitting some esoteric features, then so be it.  We also want a product that is fun and enjoyable for developers to work on, and that also provides incentive to not include the &quot;fire-and-forget&quot; code that composed many of the less-central features of Amarok 1.&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Wed, 21 Jan 2009 00:50:39 +0000</pubDate>
 <dc:creator>hydrogen</dc:creator>
 <guid isPermaLink="false">604 at https://amarok.kde.org</guid>
 <comments>https://amarok.kde.org/de/node/604#comments</comments>
</item>
<item>
 <title>It was just a beginning</title>
 <link>https://amarok.kde.org/de/node/597</link>
 <description>&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot;&gt;&lt;p&gt;This is the first time I&#039;ve actually gotten around to posting anything on the internet.  I&#039;ve been working on Amarok since we started on the 2.0 adventure, and have stuck my nose in most of Amarok since then.  I&#039;m hoping to write a series of posts about some of the cool things in Amarok 2, and some of the cooler things to come.  Today I&#039;m going to preview 2.0.1, because as we said earlier, 2.0 was just a beginning, and the evolution is already going full speed. &lt;/p&gt;

&lt;p&gt;Amarok&#039;s code was in a feature freeze for a while before 2.0 was released.  There were all sorts of changes in the code, and all sorts of annoying bugs that needed to be fixed.  Fixing bugs is no where near as fun as adding new features, and we needed to stop adding features to let the code settle down.  This left Amarok 2 missing some very useful features that Amarok 1.4 had.  The good news is, many of these features have returned in 2.0.1&lt;/p&gt;

&lt;p&gt;2.0.1 brings back searching in the playlist, as Nikolaj wrote &lt;a href=&quot;http://amarok.kde.org/blog/archives/854-Amarok-2-playlist-searching.html&quot;&gt;here&lt;/a&gt;.  Since then, Nikolaj has taken it a step further and added filtering as well as searching, which provides the opportunity to use it much as it was used in 1.4.  Seb has also merged his support for queueing, another big feature that was missing in 2.0.   In addition, &quot;Stop after Current track&quot; functionality is slowing making it&#039;s way back into svn.  This is one of those features that is much more useful that it sounds and I&#039;ve been missing it since it disappeared during 2.0 development.&lt;/p&gt;

&lt;p&gt;In addition, media device support is improving.  Alejandro has been making all sorts of improvements to ipod and mtp device support.  We have plans to add support for generic media devices, and probably others as well.  This is a great opportunity for new contributers, as this code is fairly self-contained and requires a device to use (and test) it.  If anyone is interested in contributing to Amarok, this would be a great place to start.&lt;/p&gt;

&lt;p&gt;I&#039;ve re-added custom sorting to the collection browser as well.  This is something that I don&#039;t use all that much, but it&#039;s nice to be able to change the sorting order in the collection.  In addition, we&#039;ve fixed somewhere between fifteen and twenty-five bugs already, and will probably fix more before 2.0.1 is officially released in a few weeks.&lt;/p&gt;

&lt;p&gt;The other place my focus has been recently is on the windows version of Amarok.  It&#039;s definitely usable now.  I&#039;ve fixed most of the annoying issues, and I can use it as my primary music player for local and remote music.  I&#039;ll end this post with a screenshot of Amarok 2.0.1 on windows, and encourage you to try it!&lt;/p&gt;

&lt;img src=&quot;http://www.notyetimplemented.com/images/amarok-2.0.1-blog1.jpg&quot; /&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Sat, 27 Dec 2008 17:58:41 +0000</pubDate>
 <dc:creator>hydrogen</dc:creator>
 <guid isPermaLink="false">597 at https://amarok.kde.org</guid>
 <comments>https://amarok.kde.org/de/node/597#comments</comments>
</item>
<item>
 <title>Test Blog</title>
 <link>https://amarok.kde.org/de/node/528</link>
 <description>&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot;&gt;&lt;p&gt;Testing the blogging feature.&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Mon, 14 Jul 2008 15:39:32 +0000</pubDate>
 <dc:creator>Ian</dc:creator>
 <guid isPermaLink="false">528 at https://amarok.kde.org</guid>
 <comments>https://amarok.kde.org/de/node/528#comments</comments>
</item>
<item>
 <title>Let’s be dynamic again!</title>
 <link>https://amarok.kde.org/de/node/521</link>
 <description>&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot;&gt;&lt;p&gt;Hands up in the air everyone! Please cheer for Daniel! &lt;img src=&#039;http://blog.lydiapintscher.de/wp-includes/images/smilies/icon_wink.gif&#039; alt=&#039;;-)&#039; class=&#039;wp-smiley&#039; /&gt;&lt;br /&gt;
Dynamic playlists are back \o/&lt;/p&gt;
&lt;p&gt;Daniel, my Summer of Code student, has been working hard to get one of the most loved features of Amarok 1.4 back for Amarok 2 and probably made a lot of people very happy by doing that last week. He implemented a dynamic mode as basis for the &lt;a href=&quot;http://blog.lydiapintscher.de/2008/04/24/gsoc-biased-dynamic-playlists-in-amarok-2-love/&quot;&gt;biased playlists&lt;/a&gt; he will be working on next. First results can be seen now and it is going to be great. It already improved a lot over what we had in Amarok 1.4 because it is easier to discover, configure and use. And I am sure Daniel will continue to improve it and kick ass &lt;img src=&#039;http://blog.lydiapintscher.de/wp-includes/images/smilies/icon_wink.gif&#039; alt=&#039;;-)&#039; class=&#039;wp-smiley&#039; /&gt; &lt;/p&gt;
&lt;p&gt;&lt;em&gt;You can read more about it in &lt;a href=&quot;http://kopophex.blogspot.com/2008/06/ive-been-struggling-with-how-to.html&quot;&gt;his status report for this week&lt;/a&gt; and of course try it yourself with Neon or your own build. Go read it!&lt;br /&gt;
&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;/me is proud and so happy she can listen to music again without having to select songs herself all the time&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Mon, 30 Jun 2008 22:57:54 +0000</pubDate>
 <dc:creator>nightrose</dc:creator>
 <guid isPermaLink="false">521 at https://amarok.kde.org</guid>
 <comments>https://amarok.kde.org/de/node/521#comments</comments>
</item>
<item>
 <title>Amarok 2: Artwork is Back</title>
 <link>https://amarok.kde.org/de/node/519</link>
 <description>&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot;&gt;&lt;p&gt;My last couple of weeks has been spent focusing on getting cover art back up to scratch in Amarok 2. Cover art really adds a lot of spice and colour into the UI of the application, especially now that we are showing the artwork in the collection browser, context view and playlist. For those of you who are supremely organised, the collection scanner will now trawl through directories and try to pick up images it thinks are relevant to the songs which are being scanned. Embedded artwork is not implemented currently but it is certainly on the todo list.&lt;/p&gt;
&lt;p&gt;Most of the relevant actions can now be executed for artwork: fetching from amazon, setting a custom image, removal and full size display. Music lovers with non English songs can now rejoice because we’ve also fixed some problems fetching artwork for album/artists that have accented characters, which Amazon seemed to have problems with. Another cool feature that we’ve experimented with is automatic cover fetching if there is an album with no artwork. Keep in mind that this is entirely tentative, as we are not sure that the false-positive rate from Amazon is low enough to justify polluting your database with random covers (although this will improve with our recent fix to non English tags). Maybe we’ll keep it, maybe we won’t, but it certainly is a cool feature which I’m loving at the moment - I am really lazy and hate having to explicitly fetch a cover, even though artwork is great to have.&lt;/p&gt;
&lt;div align=&#039;center&#039;&gt;&lt;a href=&#039;http://www.sebruiz.net/wp-content/uploads/a2-collection.jpeg&#039;&gt;&lt;img src=&quot;http://www.sebruiz.net/wp-content/uploads/a2-collection.jpeg&quot; alt=&quot;Collection Browser Artwork&quot; title=&quot;Collection Browser Artwork&quot; width=&quot;453&quot; height=&quot;365&quot; class=&quot;aligncenter size-full wp-image-339&quot; /&gt;&lt;/a&gt;&lt;br /&gt;Here you can see albums with artwork as well as a full size cover display&lt;/div&gt;
&lt;p&gt;In other news, I did some more work migrating statistics from Amarok 1.4 databases to the new and improved A2 schema. Here you can see how the play count, score and first/last played date for this track. Migration of lyrics and actual cover art isn’t yet implemented but that, like everything else is also on the cards.&lt;/p&gt;
&lt;div align=&#039;center&#039;&gt;&lt;a href=&#039;http://www.sebruiz.net/wp-content/uploads/a2-stats.jpeg&#039;&gt;&lt;img src=&quot;http://www.sebruiz.net/wp-content/uploads/a2-stats.jpeg&quot; alt=&quot;Track Statistics&quot; title=&quot;Track Information&quot; width=&quot;500&quot; height=&quot;223&quot; class=&quot;aligncenter size-full wp-image-338&quot; /&gt;&lt;/a&gt;&lt;/div&gt;
&lt;p&gt;Oh - one more thing:&lt;br /&gt;
My tickets are finalised, see you in Belgium in August!&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Fri, 27 Jun 2008 00:54:57 +0000</pubDate>
 <dc:creator>sebr</dc:creator>
 <guid isPermaLink="false">519 at https://amarok.kde.org</guid>
 <comments>https://amarok.kde.org/de/node/519#comments</comments>
</item>
<item>
 <title>Popup Dropper -- now with 400% more droppinness!</title>
 <link>https://amarok.kde.org/de/node/518</link>
 <description>&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot;&gt;&lt;br /&gt;
&lt;p&gt;First off -- the PUD now lives in the KDE Subversion repository!  It&#039;s at svn://anonsvn.kde.org/home/kde/trunk/playground/libs/popupdropper -- it builds with CMake and acts as a standalone library.  Included in there is a testapp that you can build with qmake that I use for my testing and that you can use to check it out, which I encourage.  Amarok now uses the PUD pulled in as a svn:external, which means Amarok is now very easily synced with updates I make to the PUD.&lt;/p&gt;&lt;p&gt;Now, on to the subject line -- the PUD now contains an average of 400% more droppiness.  That&#039;s because you can now drop on the text of an item instead of just the icon, which means a far larger drop target (and apparently this is how people expected it to behave in the first place).&lt;/p&gt;&lt;p&gt;Next on the agenda is to have text change color on hover so you&#039;re sure of what you&#039;re dropping onto.  Complete with a nice looking fade, of course...&lt;/p&gt;&lt;p /&gt; &lt;br /&gt;&lt;a href=&quot;http://amarok.kde.org/blog/archives/707-Popup-Dropper-now-with-400-more-droppinness!.html#extended&quot;&gt;Continue reading &quot;Popup Dropper -- now with 400% more droppinness!&quot;&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Thu, 26 Jun 2008 14:39:03 +0000</pubDate>
 <dc:creator>jefferai</dc:creator>
 <guid isPermaLink="false">518 at https://amarok.kde.org</guid>
 <comments>https://amarok.kde.org/de/node/518#comments</comments>
</item>
<item>
 <title>A Letter</title>
 <link>https://amarok.kde.org/de/node/517</link>
 <description>&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot;&gt;&lt;p&gt;Dear Poisonous People&lt;/p&gt;
&lt;p&gt;Please fuck off.&lt;/p&gt;
&lt;p&gt;Best,&lt;br /&gt;
sebr&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Wed, 25 Jun 2008 23:30:25 +0000</pubDate>
 <dc:creator>sebr</dc:creator>
 <guid isPermaLink="false">517 at https://amarok.kde.org</guid>
 <comments>https://amarok.kde.org/de/node/517#comments</comments>
</item>
<item>
 <title>Amarok Scripting SoC Project - Week 3</title>
 <link>https://amarok.kde.org/de/node/516</link>
 <description>&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot;&gt;My SoC student Peter Zhou has interesting news about his progress with Amarok 2&#039;s all-new scripting system. I&#039;m pasting his blog here, as he is not yet aggregated on Planet KDE.&lt;br /&gt;
&lt;br /&gt;
Before that, let me fill you in with the details about our plans for scripting in Amarok 2:&lt;br /&gt;
&lt;ul&gt;&lt;br /&gt;
&lt;li&gt;Amarok scripts are based on QtScript (= JavaScript)&lt;/li&gt;&lt;br /&gt;
&lt;li&gt;Scripts are running in-process, as opposed to Amarok 1&#039;s way of running them in a separate process&lt;/li&gt;&lt;br /&gt;
&lt;li&gt;The new script manager will be able to do version management, and possibly automatic upgrades&lt;/li&gt;&lt;br /&gt;
&lt;li&gt;The scripting API will be much richer and it will be possible to extend the Amarok GUI with scripting&lt;/li&gt;&lt;br /&gt;
&lt;/ul&gt;&lt;br /&gt;
What you can expect is something similar to Firefox&#039;s extensions. With one big difference: Amarok still aims to provide a rich feature set &lt;em&gt;out of the box.&lt;/em&gt; We believe that an application should be usable without forcing the user to do Lego (TM) building &lt;img src=&quot;http://amarok.kde.org/blog/templates/default/img/emoticons/smile.png&quot; alt=&quot;:-)&quot; style=&quot;display: inline; vertical-align: bottom;&quot; class=&quot;emoticon&quot; /&gt;&lt;br /&gt;
&lt;br /&gt;
&lt;br /&gt;
Peter writes:&lt;br /&gt;
&lt;em&gt;&lt;br /&gt;
I’ve been at home for three weeks, was with my family and had a three-weeks-leisure-break.&lt;br /&gt;
&lt;br /&gt;
Finally, I am sitting here to talk about my summer of code project. I am sorry about the first three weeks break, I really do. But I did try to get familiar with the development environment and tried to hack some code. I am going back to campus in Hong Kong in two days, I can thus concentrate on my SoC project.&lt;br /&gt;
&lt;br /&gt;
For a long time, I was trying to understand what is going on there. Trying to think what the other developers think. For the first month I joined the community, I was amazed that Amarok folks are so in love with what they are doing, and have so much passion on it. Different from my past projects, Amarok is a rather large project, different developers had different views on the future way.&lt;br /&gt;
&lt;br /&gt;
For the first time, I am feeling myself being pulled to the bleeding edge. I compiled QT for four times in two different platforms (How many times for kdelibs and kdeRunTime? &lt;img src=&quot;http://amarok.kde.org/blog/templates/default/img/emoticons/smile.png&quot; alt=&quot;:-)&quot; style=&quot;display: inline; vertical-align: bottom;&quot; class=&quot;emoticon&quot; /&gt;). I realized it is sure a though learning process. Playing with the fresh new hot stuffs, I am pretty happy with this.&lt;br /&gt;
&lt;br /&gt;
In the first week, I was busy with my exams, and cleaned up the existing dbus interface.&lt;br /&gt;
&lt;br /&gt;
For the second and third week, I had a slight trip with my girl friend, set up a new Leopard development environment, tested the MPRIS support, and made my first commitment to KDE svn server.&lt;br /&gt;
&lt;br /&gt;
I did some paper work, studied a little with scripts, and I am now quite clear with my goals for the coming busy July.&lt;br /&gt;
&lt;br /&gt;
I made my mind to immigrate everything to qtscript from dbus. I would keep the MPRIS stuffs (PlayerDBusHandler, RootDBusHandler, TracklistDBusHandler) for dbus interface. And the other functions will be scriptable through qtscript. (both ruby and python need additional runtime dependencies, but not qtscript. The simpler the better &lt;img src=&quot;http://amarok.kde.org/blog/templates/default/img/emoticons/smile.png&quot; alt=&quot;:-)&quot; style=&quot;display: inline; vertical-align: bottom;&quot; class=&quot;emoticon&quot; /&gt;)&lt;br /&gt;
&lt;br /&gt;
Compare to the current functions, I will add more signals since the signal mechanism are rather easy to be achieved using slots and signals. For example, signals like trackEnd ,trackChange, SeekingTime, configurationChange and etc. would be added.&lt;br /&gt;
&lt;br /&gt;
The second change I will make is the scriptable GUI. You will be able to add buttons, menus, lists using scripts.&lt;br /&gt;
&lt;br /&gt;
Before my visiting to Belgium, I will make a easier use script manager which include upgrade checking, simple dependency checking (to check Amarok version and optional packages for Amarok which will be also needed by scripts).&lt;br /&gt;
&lt;br /&gt;
I am so looking forward to the coming working days and nights. Hopefully, I can work out a brand new scripting interface in one and half months and thus I can start a new script project during my visit to Europe.&lt;br /&gt;
&lt;/em&gt;&lt;br /&gt;
&lt;br /&gt;
&lt;br /&gt;
Peter&#039;s original blog can be found &lt;a href=&quot;http://www.peterzl.net/en/?p=90&quot; &gt;here&lt;/a&gt;.&lt;br /&gt;
 &lt;br /&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Wed, 25 Jun 2008 14:15:46 +0000</pubDate>
 <dc:creator>markey</dc:creator>
 <guid isPermaLink="false">516 at https://amarok.kde.org</guid>
 <comments>https://amarok.kde.org/de/node/516#comments</comments>
</item>
<item>
 <title>Project Neon - Ask Harald (FAQ)</title>
 <link>https://amarok.kde.org/de/node/515</link>
 <description>&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot;&gt;Note to me, myself and I: FAQs should be listed somewhere to save me the daily headache....&lt;br /&gt;&lt;br /&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-style: italic;&quot;&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-weight: bold;&quot;&gt;Is amarok-nightly for openSUSE published yet?&lt;br /&gt;&lt;/span&gt;&lt;/span&gt;No, I&#039;ll try to get a repository outside of my personal home one and build for openSUSE 11.0, after a couple of tests amarok-nightly for openSUSE can go live.&lt;br /&gt;&lt;br /&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-style: italic; &quot;&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-weight: bold;&quot;&gt;Why is there no amarok-nightly for openSUSE 11.0 to be tested?&lt;br /&gt;&lt;/span&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-style: normal;&quot;&gt;Because I am preparing for exams, so I have no time for build-dependency magic, and because I will only build for openSUSE 11.0 once the above stated reason is sorted out. However, apparently the 10.3 packages work just fine on 11.0 as well (hooray for good packagin ;-)&lt;br /&gt;&lt;/span&gt;&lt;/span&gt;&lt;br /&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-style: italic;&quot;&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-weight: bold;&quot;&gt;Why is amarok-nightly for openSUSE not built nightly?&lt;br /&gt;&lt;/span&gt;&lt;/span&gt;No neon package gets built nightly as long as it is being tested. Nightly builds will start once openSUSE packages are published in an own repository.&lt;br /&gt;&lt;br /&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-style: italic; &quot;&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-weight: bold;&quot;&gt;Does kde-nightly for Kubuntu include debug symbols?&lt;br /&gt;&lt;/span&gt;&lt;/span&gt;No, neither does amarok-nightly. The packages are built with debugging enabled, a packaging script however strips them for some reason I forgot. In fact I tried adding -dbg packages earlier on, but that didn&#039;t work for some strange reason. The good news however is: I will digg into this soonish. After all a good description on how to reproduce is almost as good as a good backtrace ;-)&lt;br /&gt;&lt;br /&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-style: italic;&quot;&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-weight: bold;&quot;&gt;Can we get a foobar package for kde-nightly?&lt;br /&gt;&lt;/span&gt;&lt;/span&gt;Feel free to drop ideas at the mailing list, but keep in mind that the build resources on Launchpad are pretty limited, so kdepim or koffice are probably not going to get in... at least for now. In general the current amount of packages is not going to grow a lot - I might only add kdeplasmoids some time soon, but that&#039;s about it.&lt;br /&gt;&lt;br /&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-style: italic; &quot;&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-weight: bold;&quot;&gt;Do you plan to integrate more applications into kde-nightly?&lt;br /&gt;&lt;/span&gt;&lt;/span&gt;See above.&lt;br /&gt;&lt;br /&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-style: italic; &quot;&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-weight: bold;&quot;&gt;How do I compile stuff against kde-nightly?&lt;br /&gt;&lt;/span&gt;&lt;/span&gt;This is a bit tricky - you can take a look at the neonmake script in amarok-nightly-tools to get an idea. I will include some proper buildscripts later on for use with kde-nightly and amarok-nightly.&lt;br /&gt;&lt;br /&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-style: italic;&quot;&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-weight: bold;&quot;&gt;Why is there no kde-nightly for openSUSE?&lt;/span&gt;&lt;/span&gt;&lt;span class=&quot;Apple-style-span&quot; style=&quot;font-weight: bold;&quot;&gt;&lt;br /&gt;&lt;/span&gt;The openSUSE KDE guys do a great job on maintaining their own KDE snapshot packages. So launching a kde-nightly stack would be rather pointless, right? Duplicated effort and all..&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Mon, 23 Jun 2008 15:27:00 +0000</pubDate>
 <dc:creator>apachelogger</dc:creator>
 <guid isPermaLink="false">515 at https://amarok.kde.org</guid>
 <comments>https://amarok.kde.org/de/node/515#comments</comments>
</item>
</channel>
</rss>
